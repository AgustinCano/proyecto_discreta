#ifndef _READ_H
#define _READ_H

#include "Types.h"

lado ReadLineCyP(void);
/*
 * Esta funcion consume las lineas comentario
 * y las elimina para poder seguir leyendo el
 * archivo. Una vez llegamos a la linea p, 
 * consumimos los caracteres 'edge' y 
 * guardamos los dos parametros restantes que
 * seran, el numero de vertices y el numero
 * de lados.
 */

lado ReadLinesE(void);
/*
 * Crea una estructura de lado y la completa
 * con los dos numeros con los que se 
 * encuentra al consumir otra linea que 
 * empieza con e.
 */

#endif
