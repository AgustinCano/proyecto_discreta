#ifndef _BINARYSEARCH_H
#define _BINARYSEARCH_H

#include "Types.h"

u32 BinSear(Grafo G, u32 ID, u32 i, u32 j);

u32 BinarySearch(Grafo G, u32 ID, u32 length);

u32 VertExist(Grafo G, u32 ID, u32 f);

#endif
