#include "Vertices.h"

u32 NombreDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->num_vertices)
		return MAX_U32;

	u32	nombre = G->vertices[i]->ID;
	return nombre;
}

u32 ColorDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->num_vertices)
		return MAX_U32;

	u32	color = G->vertices[i]->color;
	return color;
}

u32 GradoDelVertice(Grafo G, u32 i) {

  // Chequear i correcto
	if(i >= G->num_vertices)
		return MAX_U32;

	u32	grado = G->vertices[i]->cant_vecinos;
	return grado;
}

u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j) {

  // Chequear i correcto
	if(i >= G->num_vertices)
		return MAX_U32;

  // Chequear j correcto
	if(j >= G->vertices[i]->cant_vecinos)
		return MAX_U32;

	u32	colorj = G->vertices[i]->vecinos[j]->color;
	return colorj;
}

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j) {

  // Chequear i correcto
	if(i >= G->num_vertices)
		return MAX_U32;

  // Chequear j correcto
	if(j >= G->vertices[i]->cant_vecinos)
		return MAX_U32;

	u32	nombrej = G->vertices[i]->vecinos[j]->ID;
	return nombrej;
}
