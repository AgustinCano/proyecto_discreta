#ifndef _SWITCH_H
#define _SWITCH_H

#include "Types.h"

char SwitchVertices(Grafo G, u32 i, u32 j);
/* 
 * Esta funcion cambia el vertice en la posicion i
 * por el vertice en la posicion j del arreglo de
 * vertices con el orden actual.
 */

char SwitchColores(Grafo G, u32 i, u32 j);
/*
 * Pinta de color i todos los vertices de color j
 * y viceversa.
 */

#endif
