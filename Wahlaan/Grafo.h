#ifndef _GRAFO_H
#define _GRAFO_H

#include "Types.h"

Grafo ConstruccionDelGrafo(void);
/*
 * Aloca memoria y lee el formato de DIMACS para
 * inicializar la estructura del grafo. Si en 
 * algun momento ocurre un problema en esta etapa
 * imprimimos errores.
 * Lo primero que hacemos es copiar los lados del
 * grafo en un arreglo. Despues los ordenamos en
 * el orden natural ascendente. A continuacion,
 * vamos inicializando vertices y, una vez que
 * terminamos, agregamos los vecinos de cada 
 * vertice con un puntero apuntando al vertice
 * original. En todo momento chequeamos por errores
 * Finalmente devolvemos un puntero a la estructura
 * cargada.
 */

void DestruccionDelGrafo(Grafo G);
/*
 * Destruye el grafo G y libera toda la memoria
 * alocada.
 */

Grafo CopiarGrafo(Grafo G);
/*
 * Copia el grafo G para hacer checkeos. Devuelve
 * un puntero a la copia nueva.
 */

u32 NumeroDeVertices(Grafo G);
/*
 * Devuelve el numero de vertices de G.
 */

u32 NumeroDeLados(Grafo G);
/*
 * Devuelve el numero de lados de G.
 */

u32 NumeroDeColores(Grafo G);
/*
 * Devuelve el numero de colores de G.
 */

#endif
