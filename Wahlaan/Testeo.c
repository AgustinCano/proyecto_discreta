#include "Rii.h"

int main(void) {

    time_t t, total;

    total = time(NULL);

    t = time(NULL);
    Grafo G = ConstruccionDelGrafo();

    if(G == NULL) {
        printf("No existe el archivo\n");
        return 1;
    }
    printf("ContruccionDelGrafo: %ld segs\n", time(NULL) - t);

    t = time(NULL);
    printf("El grafo G ");
    if(Bipartito(G) == 0) {
        printf("no es bipartito\n");
    } else {
        printf("es bipartito\n");
    }
    printf("Ver si G es bip: %ld segs\n", time(NULL) - t);

    t = time(NULL);
    Grafo H = CopiarGrafo(G);
    printf("CopiarGrafo: %ld segs\n", time(NULL) - t);

    t = time(NULL);
    OrdenWelshPowell(H);
    printf("Ordenar H con WelshPowell: %ld segs\n", time(NULL) - t);

    t = time(NULL);
    H->num_colores = Greedy(H);
    G->num_colores = Greedy(G);
    printf("Colorear G y H: %ld segs\n", time(NULL) - t);

    printf("Grafo G, en orden natural, se pinto con %" PRIu32 " colores\n", G->num_colores);
    printf("Grafo H, en orden welshpowell, se pinto con %" PRIu32 " colores\n", H->num_colores);

    t = time(NULL);
    for(u32 j = 0; j < 100; j++) {
        u32 i = (u32)rand() % G->num_vertices;
        u32 k = (u32)rand() % G->num_vertices;
        SwitchVertices(G, i, k);

        i = (u32)rand() % H->num_vertices;
        k = (u32)rand() % H->num_vertices;
        SwitchVertices(H, i, k);

        G->num_colores = Greedy(G);
        H->num_colores = Greedy(H);
    }
    printf("Hacer 100 SV en G y en H: %ld segs\n", time(NULL) - t);

    printf("Luego de 100 SwitchVertices, se pinto a G con %" PRIu32 " colores\n", G->num_colores);
    printf("Luego de 100 SwitchVertices, se pinto a H con %" PRIu32 " colores\n", H->num_colores);

    t = time(NULL);
    for(u32 j = 0; j < 1000; j++) {
        RMBCchicogrande(G);
        RMBCrevierte(H);

        G->num_colores = Greedy(G);
        H->num_colores = Greedy(H);
    }
    printf("Hacer 1000 RMBC en G y en H: %ld segs\n", time(NULL) - t);

    printf("Luego de 1000 RMBCchicogrande se pinto a G con %" PRIu32 " colores\n", G->num_colores);
    printf("Luego de 1000 RMBCrevierte se pinto a H con %" PRIu32 " colores\n", H->num_colores);

    t = time(NULL);
    DestruccionDelGrafo(G);
    DestruccionDelGrafo(H);
    printf("Destruir G y H: %ld segs\n", time(NULL) - t);

    printf("Tiempo total en minutos: %ld minutos\n", (time(NULL) - total)/60);

    return 0;
}
