#include "Read.h"

char *ReadLine(void) {
    size_t sz = 16;
    size_t used = 0;
    char *line = NULL;

    while(1) {
        char *tmp = realloc(line, sz * sizeof(char));
        if (NULL == tmp) {
            free(line);
            return NULL;
        }
        line = tmp;

        if (NULL == fgets(line + used, (int)(sz - used), stdin)) {
            free(line);
            return NULL;
        }

        used = strlen(line);

        if (feof(stdin))
            return line;

        if ('\n' == line[used-1]) {
            line[used-1] = '\0';
            return line;
        }

        sz += sz/2;
    }
}

lado ReadLineCyP() {
    u32 x, y;
    char c;

  // Alocar memoria para el lado que tendra la cant. de vertices y lados
    lado res = malloc(sizeof(ladost));

  // Consumir lineas
    char *charr = ReadLine();
    if (NULL == charr) {
        free(res);
        free(charr);
        return NULL;
    }

    while(charr[0] == 'c') {
        free(charr);
        charr = ReadLine();
        if (NULL == charr) {
            free(res);
            free(charr);
            return NULL;
        }
    }

  // Escanear numero de vertices y lados
    sscanf(charr, "%c %*c %*c %*c %*c %" PRIu32 " %" PRIu32, &c, &x, &y);

    free(charr);

  // Guardar en el lado res y retornar
    if(c == 'p') {
        res->x = x;
        res->y = y;
    }
    return res;
}

lado ReadLinesE() {
    u32 x, y;
    lado res = malloc(sizeof(ladost));

  // Alocar memoria para consumir linea y luego consumirla
    char *charr = ReadLine();
    if (NULL == charr) {
        free(charr);
        free(res);
        return NULL;
    }

  // Escanear IDs del futuro lado
    sscanf(charr, "%*c %" PRIu32 " %" PRIu32, &x, &y);

    free(charr);

  // Establecer lado y retornar
    res->x = x;
    res->y = y;
    return res;
}
