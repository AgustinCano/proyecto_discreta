#include "BusquedaBinaria.h"

u32 BinSear(Grafo G, u32 ID, u32 i, u32 j) {
    if(i <= j && j != MAX_U32) {
        u32 mid = (i + j) / 2;
        if(G->vertices[mid]->ID == ID)
            return mid;
        if(G->vertices[mid]->ID > ID) {
            return BinSear(G, ID, i, mid-1);
        } else {
            return BinSear(G, ID, mid+1, j);
        }
    } else {
        return MAX_U32;
    }
}

u32 BinarySearch(Grafo G, u32 ID, u32 length) {
    u32 res = BinSear(G, ID, 0, length);
    return res;
}

u32 VertExist(Grafo G, u32 ID, u32 length) {
    u32 k = MAX_U32;
    for(u32 i = 0; i < length; i++) {
        if(G->vertices[i]->ID == ID)
           k = i;
    }
    return k;
}
