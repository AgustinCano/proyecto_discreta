#include "Switch.h"

char SwitchVertices(Grafo G, u32 i, u32 j) {

  // Comprobar i, j en rango
	if(i > G->num_vertices || j > G->num_vertices)
		return 1;

    vertice tmp = G->vertices[i];
    G->vertices[i] = G->vertices[j];
    G->vertices[j] = tmp;

	return 0;
}

char SwitchColores(Grafo G, u32 i, u32 j) {

  // Comprobar i, j en rango
	if(i > G->num_colores || j > G->num_colores) {
		return 1;
	}

  // Recorrer vertices
	for(u32 k = 0; k < G->num_vertices; k++) {

      // Pintar de color j los vertices pintados con color i
		if(G->vertices[k]->color == i)
			G->vertices[k]->color = j;

      // Pintar de color i los vertices pintados con color j
		if(G->vertices[k]->color == j)
			G->vertices[k]->color = i;
	}
	return 0;
}

