#ifndef _TYPES_H
#define _TYPES_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <time.h>

#define MAX_U32 4294967295

typedef uint32_t u32;               //u32 es un entero sin signo de 32 bits.
typedef uint64_t u64;

typedef struct ladost {             //Estructura de lados.
    u32 x;                          //Coordenada x.
    u32 y;                          //Coordenada y.
} ladost;

typedef ladost* lado;               //Puntero a la estructura lado.

typedef struct verticest {          //Estructura de vertices.
	u32 ID;							//ID de cada vertice.
	u32 color;						//Color actual, inicializado en MAX_U32.
	u32 cant_vecinos;				//Nos es util guardar cuantos vecinos hay.
	struct verticest* *vecinos;	    //Arreglo con todos los vecinos del vertice.
} verticest;

typedef verticest* vertice;			//Puntero a la estructura de vertices.

typedef struct GrafoSt {            //Estructura del grafo.
	u32 num_vertices;				//Numero de vertices del grafo.
	u32 num_lados;					//Numero de lados del grafo.
	u32 num_colores;				//Numero de colores con los que se coloreo el grafo.
    u32 delta;                      //Grado del vertice con mas vecinos.
	vertice *vertices;				//Arreglo de vertices.
    lado *lados;                    //Arreglo de lados originales y lados invertidos.
} GrafoSt;

typedef GrafoSt* Grafo;				//Puntero a la estructura de grafos.

typedef struct AuxSt {
    u32 count;
    u32 size;
    vertice *vertices;
} AuxSt;

typedef AuxSt* Aux;

#endif
