#ifndef _COLOREO_H
#define _COLOREO_H

#include "Types.h"

u32 Greedy(Grafo G);
/*
 * Agarrar uno por uno los vertices del arreglo
 * de vertices y pintarlos con el menor color
 * que este disponible. El color esta disponible
 * si ninguno de los vecinos esta pintado de ese
 * color. Como los colores se inicializan en 
 * MAX_U32 el primer vertice tendra color 0.
 * Ahora seguimos haciendo esto con el resto de 
 * vertices. En todo el proceso guardamos el color
 * con el que pintamos en la estructura de G, y 
 * todo el tiempo vemos si el color es mas grande 
 * que con el que estamos pintando. De esta forma  
 * al final del proceso en esta variable tendremos
 * la cantidad de colores con la que se pinto G.
 */

int Bipartito(Grafo G);
/*
 * Para ver si un grafo es bipartito lo que 
 * hacemos es..
 */

#endif
