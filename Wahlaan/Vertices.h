#ifndef _VERTICES_H
#define _VERTICES_H

#include "Types.h"

u32 NombreDelVertice(Grafo G, u32 i);
/*
 * Devuelve el ID del vertice en la posicion i del
 * arreglo de vertices en el orden actual.
 */ 

u32 ColorDelVertice(Grafo G, u32 i);
/*
 * Devuelve el color del vertice en la posicion i
 * del arreglo de vertices en el orden actual.
 */ 

u32 GradoDelVertice(Grafo G, u32 i);
/*
 * Devuelve la cantidad de vecinos del vertice en 
 * la posicion i del arreglo de vertices en el 
 * orden actual.
 */

u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j);
/*
 * Devuelve el color del vecino j del vertice en
 * la posicion i del arreglo de vertices en el 
 * orden actual.
 */

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j);
/*
 * Devuelve el ID del vecino j del vertice en
 * la posicion i del arreglo de vertices en el 
 * orden actual.
 */

#endif
