#include "Bfs.h"

int BFS(Grafo G, u32 posFirstV) {

    int res = 0;
    u32 Ccontrario = 1;

  // Reservar memoria para el arreglo que servira de cola
    vertice *queue = malloc(G->num_vertices * sizeof(vertice));

  // Variables para dar semantica de cola al arreglo queue
    u32 count = 0;
    u32 dequeue = 0;

    queue[dequeue] = G->vertices[posFirstV];
    queue[dequeue]->color = 0;

    while(dequeue < G->num_vertices && (dequeue <= count )) {

      // Al tomar el primer vertice de la cola, buscar color para pintar vecinos
        if(queue[dequeue]->color == 0) {
            Ccontrario = 1;

        } else {
            Ccontrario = 0;
        }

        for(u32 i = 0; i < queue[dequeue]->cant_vecinos; i++) {

          // Si el vertice no ha sido visitado lo agregamos a la cola y pintamos
            if(queue[dequeue]->vecinos[i]->color == MAX_U32) {
                count++;
                queue[count] = queue[dequeue]->vecinos[i];
                queue[count]->color = Ccontrario;

            } else {

              // Vertice visitado tiene mismo color que un vecino, retornar 1
                if(queue[dequeue]->color == queue[dequeue]->vecinos[i]->color) {
                    res = 1;
                    free(queue);
                    return res;
                }
            }
        }

        dequeue++;
    }

    free(queue);
    return res;
}
