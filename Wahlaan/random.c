#include <assert.h>
#include <stdlib.h>

#include "random.h"

#define DEFAULT_N 16

struct random_state_t {
    u64 *s;
};

/* Create a state */
rndSt rndInit(rndElem seed) {
    assert(0 != seed);

    // Alloc memory for struct and state
    rndSt st = malloc(sizeof(struct random_state_t));
    if (NULL == st)
        return NULL;

    u64 *newS = malloc(2 * sizeof(u64));
    if (NULL == newS)
        return NULL;

    // Get seeds to have a more or less balanced amount of 0s and 1s
    // These specific numbers perhaps are not the best, though they were used
    // for other purposes
    st->s = newS;
    st->s[0] = (u64)0x8a5cd789635d2dff ^ seed;
    st->s[1] = (u64)0x121fd2155c472f96 ^ seed;

    return st;
}

rndSt rndCreate(rndElem seed) {
    assert(0 != seed);

    rndSt st = rndInit(seed);

    // A few iterations first to improve performance
    for (rndElem i = 0; i < DEFAULT_N; i++)
        rndGet(st);

    return st;
}

void rndDestroy(rndSt st) {
    assert(NULL != st && NULL != st->s);

    free(st->s);
    free(st);
}

rndElem rndGet(rndSt st) {
    assert(NULL != st && NULL != st->s);

    // Voodoo witchcraft
    u64 x = st->s[0];
    u64 y = st->s[1];
    st->s[0] = y;
    x ^= x << 23;
    st->s[1] = x ^ y ^ (x >> 17) ^ (y >> 26);

    // Lower bits are not properly random
    return (rndElem)((st->s[1] + y) >> 4);
}

int cmp(const void *x, const void *y) {
    rndElem a, b;

    a = *(rndElem *)x;
    b = *(rndElem *)y;

    return (a > b) - (b > a);
}

/* Sets all but one copy of a number to 0
 * a must not be NULL, and has to have n places */
rndElem rndRmDup(rndElem *a, rndElem n) {
    assert(NULL != a);

    rndElem res = 0;

    // Sort a so that duplicates are together
    qsort(a, n, sizeof(rndElem), cmp);

    // Search duplicates, set all but the first to 0
    for (rndElem i = 0; i < n; i++)
        for (rndElem j = i + 1; j < n; j++)
            if (0 == cmp(&a[i], &a[j])) {
                res++;
                a[j] = 0;
            } else {
                break;
            }

    // Sort a so that 0s are first
    qsort(a, n, sizeof(rndElem), cmp);

    return res;
}

rndElem *rndArray(rndSt st, rndElem n, rndElem lim) {
    assert(NULL != st && NULL != st->s && lim >= n);

    // Get an array of n unique random numbers
    rndElem *a = malloc(n * sizeof(rndElem));
    if (NULL == a)
        return NULL;

    // If numbers can appear multiple times, there's no need ask for more
    rndElem left = n;

    // Removing the duplicates and refilling the array until finished
    while (left > 0) {
        for (rndElem i = 0; i < left; i++)
            a[i] = (rndGet(st) % lim) + 1;

        left = rndRmDup(a, n);
    }

    for (rndElem i = 0; i < n; i++)
        a[i]--;

    return a;
}
