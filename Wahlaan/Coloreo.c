#include "Coloreo.h"
#include "Bfs.h"

u32 Greedy(Grafo G) {
	u32 color_max = 0;
	char *colores = calloc(G->delta+1, sizeof(char));

  // Limpiar
	for(u32 i = 0; i < G->num_vertices; i++)
      G->vertices[i]->color = MAX_U32;

  // Colorear
	for(u32 i = 0; i < G->num_vertices; i++) {

    // Buscar colores de vecinos
		for(u32 j = 0; j < G->vertices[i]->cant_vecinos; j++) {
		    u32 color_actual = G->vertices[i]->vecinos[j]->color;
        if(color_actual != MAX_U32)
			      colores[color_actual] = 1;
		}

    // Buscar minimo color no utilizado
    // Ir limpiando los utilizados
		u32 color_min = MAX_U32;
		for(u32 j = 0; j < G->delta + 1; j++) {
			if(colores[j] != 0)
          colores[j] = 0;
            else {
				  color_min = j;
          break;
			}
		}

    // Terminar de limpiar los utilizados
    for (u32 j = color_min + 1; j < G->delta + 1; j++)
        colores[j] = 0;

    // Actualizar nro de colores y color del vertice
    if(color_min > color_max)
        color_max = color_min;

		G->vertices[i]->color = color_min;
	}

  free(colores);
  G->num_colores = color_max + 1;
	return color_max + 1;
}

int Bipartito(Grafo G) {

  // Limpiar posible coloreo previo
    for(u32 i = 0; i < G->num_vertices; i++)
      G->vertices[i]->color = MAX_U32;

    int k = BFS(G, 0);

  // Buscar en todas las componentes conexas
    for(u32 i = 0; i < G->num_vertices; i++) {

        if(G->vertices[i]->color == MAX_U32) {
            k = k + BFS(G, i);
        }
    }

    if(k == 0) {
	    return 1;

    } else {
        return 0;
    }
}
