#ifndef BFS_H
#define BFS_H

#include "Types.h"

int BFS(Grafo G, u32 posV);
/*
 * Se aloca un arreglo y se le da semantica de cola.
 * Agregamos el (primer) vertice, posV, a la cola.
 * Luego recorremos los vecino de posV (en orden BFS)
 * y si no han sido visitados aun, los pintamos y los
 * agregamos a la cola. Si han sido visitados nos
 * fijamos si tiene el mismo color que posV, si pasa
 * esto, no es bipartito, sino, seguimos con el resto
 * de los vecinos. Luego seguimos con el resto de los
 * vertices de la cola.
 * Esto nos dice si la componente conexa de posV es 
 * 2-coloreable o no.
 */

#endif
