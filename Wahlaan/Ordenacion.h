#ifndef _ORDENACION_H
#define _ORDENACION_H

#include "Types.h"

char OrdenNatural(Grafo G);
/* 
 * Esta funcion ordena los vertices por su ID
 * en orden creciente.
 */

char OrdenWelshPowell(Grafo G);
/* 
 * Esta funcion ordena los vertices por su grado
 * en orden decreciente.
 */

char RMBCnormal(Grafo G);
/*
 * Esta funcion coloca los vertices del color 0 
 * primero, luego los del color 1, etc, hasta el
 * color r-1, con r la cantidad de colores con la
 * que fue coloreado G.
 */

char RMBCrevierte(Grafo G);
/*
 * Esta funcion coloca los vertices del color r-1 
 * primero, luego los del color r-2, etc, hasta
 * el color 0, con r la cantidad de colores con la
 * que fue coloreado G.
 */

char RMBCchicogrande(Grafo G);
/*
 * Esta funcion coloca los vertices del color j0 
 * primero, luego los del color j1, etc, hasta
 * el color jr-1, con r la cantidad de colores con
 * la que fue coloreado G y j0, jr-1 el color con
 * el que fue coloreado la menor y la mayor (resp)
 * cantidad de vertices. 
 */

char Ordenar(Grafo G, char n);
/*
 * Esta funcion solo es para poder llamarla desde
 * la funcion main y poder elegir en que orden 
 * correr Greedy desde consola.
 */

#endif
