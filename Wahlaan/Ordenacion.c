#include "Ordenacion.h"

int RCompareColor(const void * c, const void * d) {
	vertice a = *(vertice*)c;
	vertice b = *(vertice*)d;

	return (a->color > b->color) - (a->color < b->color);
}

int CompareColor(const void * c, const void * d) {
	vertice a = *(vertice*)c;
	vertice b = *(vertice*)d;

	return (b->color > a->color) - (b->color < a->color);
}

int CompareID(const void * c, const void * d) {
	vertice a = *(vertice*)c;
	vertice b = *(vertice*)d;

	return (a->ID > b->ID) - (a->ID < b->ID);
}

int CompareGrado(const void * c, const void * d) {
	vertice a = *(vertice*)c;
	vertice b = *(vertice*)d;

    return (a->cant_vecinos > b->cant_vecinos) - (b->cant_vecinos > a->cant_vecinos);
}

int ChicoGrande(const void * c, const void * d) {
	Aux a = *(Aux*)c;
	Aux b = *(Aux*)d;

    return (a->count > b->count) - (b->count > a->count);
}

char OrdenNatural(Grafo G) {
	qsort(G->vertices, G->num_vertices, sizeof(vertice), &CompareID);
	return 0;
}

char OrdenWelshPowell(Grafo G) {
	qsort(G->vertices, G->num_vertices, sizeof(vertice), &CompareGrado);
	return 0;
}

char RMBCnormal(Grafo G) {
	qsort(G->vertices, G->num_vertices, sizeof(vertice), &RCompareColor);
	return 0;
}

char RMBCrevierte(Grafo G) {
	qsort(G->vertices, G->num_vertices, sizeof(vertice), &CompareColor);
	return 0;
}

char RMBCchicogrande(Grafo G) {
	Aux *color = malloc(G->num_colores * sizeof(Aux));
    for(u32 i = 0; i < G->num_colores; i++) {
        color[i] = malloc(sizeof(AuxSt));
        color[i]->count = 0;
        color[i]->size = 1024;
        color[i]->vertices = malloc(1024 * sizeof(vertice));
    }

	for(u32 k = 0; k < G->num_vertices; k++) {
        u32 pos_color = G->vertices[k]->color;

        if(pos_color != MAX_U32) {
            if (color[pos_color]->count == color[pos_color]->size) {
                color[pos_color]->size = color[pos_color]->size * 2;
                color[pos_color]->vertices = realloc(color[pos_color]->vertices,
                                             color[pos_color]->size * sizeof(vertice));
            }

            color[pos_color]->vertices[color[pos_color]->count] = G->vertices[k];
            color[pos_color]->count += 1;
        }
    }

  // Ordenar el arreglo de arreglos de vertices
    qsort(color, G->num_colores, sizeof(Aux), &ChicoGrande);

    u32 i = 0;
    for(u32 k = 0; k < G->num_colores; k++) {
        for(u32 j = 0; j < color[k]->count; j++) {
            G->vertices[i] = color[k]->vertices[j];
            i++;
        }
        free(color[k]->vertices);
        free(color[k]);
    }
    free(color);
	return 0;
}

char Ordenar(Grafo G, char n) {
    switch (n) {

        case '1':
            OrdenNatural(G);
            break;

        case '2':
            OrdenWelshPowell(G);
            break;

        case '3':
            RMBCnormal(G);
            break;

        case '4':
            RMBCrevierte(G);
            break;

        case 5:
            RMBCchicogrande(G);
            break;

        default:
            printf("No ingreso orden o no existe\n");
            return 1;
    }
    return 0;
}
