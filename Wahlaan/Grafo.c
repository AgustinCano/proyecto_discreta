#include "Grafo.h"
#include "Read.h"
#include "Coloreo.h"
#include "Lado.h"
#include "BusquedaBinaria.h"

Grafo ConstruccionDelGrafo(void) {
    Grafo G = NULL;

  // Conseguir nºlados y nºvertices
    lado VyL = ReadLineCyP();
    if(VyL == NULL)
        return G;

  // Hacer espacio para el grafo
    G = malloc(sizeof(GrafoSt));
    G->num_vertices = VyL->x;
    G->num_lados = VyL->y;
    G->delta = 0;

    free(VyL);

  // Pedir memoria para alocar 2 veces los lados
    G->lados = malloc(sizeof(lado) * 2 * G->num_lados);

  // Completar la primera mitad con los lados originales
    for(u32 i = 0; i < G->num_lados; i++) {
        lado L = ReadLinesE();
        G->lados[2*i] = L;
        G->lados[2*i+1] = RotarLado(L);
    }

  // Ordenar los lados por la primer coordenada
    OrdenarLados(G->num_lados*2, G->lados);

  // Pedir memoria para alocar los vertices
    G->vertices = malloc(sizeof(vertice) * G->num_vertices);

  // Recorrer los lados y agregar vertices
    u32 prevVert = MAX_U32; 
    u32 j = 0;
    for(u32 i = 0; i < 2 * G->num_lados; i++) {

      // Si el vertice ya existe sumar vecino y seguir
        if(G->lados[i]->x == prevVert) {
            G->vertices[j-1]->cant_vecinos = G->vertices[j-1]->cant_vecinos + 1;
            G->vertices[j-1]->vecinos = realloc(G->vertices[j-1]->vecinos,  
                                              sizeof(vertice) * G->vertices[j-1]->cant_vecinos);

              // Actualizar delta de ser necesario
                if(G->vertices[j-1]->cant_vecinos > G->delta)
                    G->delta = G->vertices[j-1]->cant_vecinos;

      // Si el vertice no existe, inicializar
        } else { 
            verticest *nuevoVert = malloc(sizeof(verticest));
            G->vertices[j] = nuevoVert;
            G->vertices[j]->cant_vecinos = 1;
            G->vertices[j]->ID = G->lados[i]->x;
            G->vertices[j]->color = MAX_U32;
            G->vertices[j]->vecinos = malloc(sizeof(vertice));

      // Pasar al siguiente y actualizar el id del vertice previo
            j++;
            prevVert = G->lados[i]->x;
        }
    }

  // Agregar vecinos 
    prevVert = MAX_U32;
    j = 0;
    u32 r = 0;
    for(u32 i = 0; i < 2 * G->num_lados; i++) {

      // Ya existe el vertice
        if(G->lados[i]->x == prevVert) {
            G->vertices[j-1]->vecinos[r] = G->vertices[BinarySearch(G, G->lados[i]->y, G->num_vertices)];
            r++;

      // No existe el vertice
        } else {
            G->vertices[j]->vecinos[0] = G->vertices[BinarySearch(G, G->lados[i]->y, G->num_vertices)];
            j++;
            r = 1;
            prevVert = G->lados[i]->x;
        }
    }
    G->num_colores = Greedy(G);
    return G;
}

void DestruccionDelGrafo(Grafo G) {
	for(u32 i = 0; i < G->num_vertices; i++) {
        free(G->vertices[i]->vecinos);
    }
	for(u32 i = 0; i < G->num_vertices; i++) {
        free(G->vertices[i]);
    }
	for(u32 i = 0; i < 2 * G->num_lados; i++) {
        free(G->lados[i]);
    }
    free(G->lados);
    free(G->vertices);
    free(G);
}

Grafo CopiarGrafo(Grafo G) {
	Grafo H = malloc(sizeof(GrafoSt));
    H->num_vertices = G->num_vertices;
    H->num_lados = G->num_lados;
    H->delta = G->delta;
    H->num_colores = G->num_colores;
    H->vertices = malloc(H->num_vertices * sizeof(vertice));
    H->lados = malloc(sizeof(lado) * 2 * G->num_lados);
    
    for(u32 i = 0; i < 2 * H->num_lados; i++) {
        lado newlado = malloc(sizeof(ladost));
        H->lados[i] = newlado;
        H->lados[i]->x = G->lados[i]->x;
        H->lados[i]->y = G->lados[i]->y;
    }

    for(u32 i = 0; i < H->num_vertices; i++) {
        vertice newvertice = malloc(sizeof(verticest));
        H->vertices[i] = newvertice;
        H->vertices[i]->ID = G->vertices[i]->ID;
        H->vertices[i]->color = G->vertices[i]->color;
        H->vertices[i]->cant_vecinos = G->vertices[i]->cant_vecinos;
        H->vertices[i]->vecinos = malloc(sizeof(vertice) * H->vertices[i]->cant_vecinos);
    }

    u32 prevVert = MAX_U32;
    u32 j = 0;
    u32 r = 0;
    for(u32 i = 0; i < 2 * H->num_lados; i++) {

        if(G->lados[i]->x == prevVert) {
            H->vertices[j-1]->vecinos[r] = H->vertices[BinarySearch(H, H->lados[i]->y, H->num_vertices)];
            r++;

        } else {
            H->vertices[j]->vecinos[0] = H->vertices[BinarySearch(H, H->lados[i]->y, H->num_vertices)];
            j++;
            r = 1;
            prevVert = H->lados[i]->x;
        }
    }
	return H;
}

u32 NumeroDeVertices(Grafo G) {
	u32 vertices = G->num_vertices;
	return vertices;
}

u32 NumeroDeLados(Grafo G) {
	u32 lados = G->num_lados;
	return lados;
}

u32 NumeroDeColores(Grafo G) {
	u32 colores = G->num_colores;
	return colores;
}
