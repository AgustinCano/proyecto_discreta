#ifndef _LADO_H
#define _LADO_H

#include "Types.h"

lado RotarLado(lado L);
/*
 * Esta funcion aloca memoria para un
 * nuevo lado y lo inicializa en base
 * al lado que tenemos como parametro
 * e invierte los ejes x e y.
 */

int Comparar(const void * c, const void * d);
/*
 * Funcion para evaluar que lado va  
 * antes que otro.
 */

char OrdenarLados(u32 length, lado L[]);
/*
 * Funcion de ordenacion para ordenar 
 * estructuras de lados. El orden es
 * el natural ascendente.
 */

#endif 
